const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const knex = require("knex")(require("./knexfile"));

const PORT = 4000;

const app = express();
app.use(bodyParser.json());
app.use(cors({ origin: "http://localhost:3000" }));

knex.on("query", ({ sql }) => console.log(sql));

app.get("/greeting", async (_, res) => {
  const [greeting] = await knex("greetings").limit(1);

  res.send(JSON.stringify(greeting));
});

app.get("/palettes", async (_, res) => {
  const palettes = await knex('palettes');

  // translate hex values to objects
  var rgb_palettes = [];
  palettes.forEach((palette) => {
    var rgb_colors = [];
    Object.values(palette).forEach((color) => {
      if (typeof color !== 'string') {return;}
      rgb_colors.push({
        red: parseInt(color.slice(0,2), 16),
        green: parseInt(color.slice(2,4), 16),
        blue: parseInt(color.slice(4,6), 16),
      });
    });
    rgb_palettes.push({id: palette.id, colors: rgb_colors});
  });

  res.send(JSON.stringify(rgb_palettes));
});

app.post('/palettes', async (req, res) => {
  var palette = req.body.palette;

  var hex_colors = [];
  palette.forEach((color) => {
    hex_colors.push(
      parseInt(color.red).toString(16).padStart(2, '0') +
      parseInt(color.green).toString(16).padStart(2, '0') +
      parseInt(color.blue).toString(16).padStart(2, '0')
    )
  });

  var hex_palette = {};
  const keys = [
    'first',
    'second',
    'third',
    'fourth',
    'fifth',
  ];
  keys.forEach((key, index) => hex_palette[key] = hex_colors[index]);

  await knex("palettes").insert(hex_palette);

  // this needs an error state
  res.send(JSON.stringify('created'));
});

app.delete('/palettes/:id', async (req, res) => {
  await knex('palettes').where('id', req.params.id).del();
  res.send(JSON.stringify('deleted'));
});

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
