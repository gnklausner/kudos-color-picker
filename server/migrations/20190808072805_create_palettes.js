
exports.up = function(knex) {
  return knex.schema.createTable("palettes", function(table) {
    table.increments("id");

    table.string('first').notNullable();
    table.string('second').notNullable();
    table.string('third').notNullable();
    table.string('fourth').notNullable();
    table.string('fifth').notNullable();
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("palettes");
};
//
