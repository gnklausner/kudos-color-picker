import Palette from "../components/palette";
import PaletteList from "../components/palette-list"
import "./styles.scss";

const Home = () => {
  return (
    <div>
      <Palette />
      <PaletteList />
    </div>
  );
};

export default Home;
