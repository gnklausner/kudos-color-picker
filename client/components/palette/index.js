import { useState } from "react";
import axios from "axios";
import { SERVER_URL } from "../../constants";
import ColorSelector from "../color-selector";
import "./styles.scss";

const Palette = () => {
  const default_colors = new Array(5).fill({
    red: 0,
    green: 0,
    blue: 0
  });

  const [colors, setColors] = useState(default_colors);

  var savePalette = async () => {
    const { status, data } = await axios.post(`${SERVER_URL}/palettes`, {
      palette: colors
    });
  }

  function updateColors(color) {
    let colors_copy = [...colors]
    colors_copy[color.index] = color.value;
    setColors(colors_copy);
  }

  return (
    <div className="color-selector-container" >
      {colors.map((color,index) => (
        <ColorSelector key={index} ndx={index} color={color} updateColor={updateColors} />
      ))}
      <button onClick={savePalette} >Save</button>
    </div>
  );
};

export default Palette;
