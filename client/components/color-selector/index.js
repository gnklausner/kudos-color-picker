import { useState, useEffect } from "react";
import "./styles.scss";

const ColorSelector = (props) => {
  function handleChange(e) {
    props.updateColor({
      value: {...props.color, [e.target.name]: e.target.value},
      index: props.ndx
    });
  }

  var color_styles = {
    background: 'rgb(' + props.color.red + ',' + props.color.green + ',' + props.color.blue + ')'
  };

  return (
    <div className="color-container">
      <div className="color-display" style={color_styles} />
      <div className="input-container">
        <div className="red">
          <input
            id="red"
            name="red"
            type="range"
            min="0"
            max="255"
            steps="1"
            value={props.color.red}
            onChange={handleChange}
          />
        </div>
        <div className="green">
          <input
            id="green"
            name="green"
            type="range"
            min="0"
            max="255"
            steps="1"
            value={props.color.green}
            onChange={handleChange}
          />
        </div>
        <div className="blue">
          <input
            id="blue"
            name="blue"
            type="range"
            min="0"
            max="255"
            steps="1"
            value={props.color.blue}
            onChange={handleChange}
          />
        </div>
      </div>
    </div>
  );
};

export default ColorSelector;
