import { useState, useEffect } from "react";
import "./styles.scss";
import axios from "axios";
import { SERVER_URL } from "../../constants";

const PaletteList = () => {
  const [palettes, setPalettes] = useState([]);

  useEffect(() => {
    const fetchPalettes = async () => {
      const { status, data } = await axios.get(`${SERVER_URL}/palettes`);

      if (status === 200) {
        setPalettes(data);
      } else {
        throw new Error("Error connecting to server");
      }
    };

    fetchPalettes();
  });

  var deletePalette = async (palette_id) => {
    const { status, data } = await axios.delete(`${SERVER_URL}/palettes/${palette_id}`);
  }

  return (
    <div className="palette-list-container" >
      {palettes.map((palette) => (
        <div className="palette" key={palette.id} >
          {palette.colors.map((color, index) => (
            <div className="color" key={index} style={{
              background: 'rgb(' +
                color.red + ',' +
                color.green + ',' +
                color.blue + ')'
            }} />
          ))}
          <button onClick={() => deletePalette(palette.id)} >DELETE</button>
        </div>
      ))}
    </div>
  );
};

export default PaletteList;
